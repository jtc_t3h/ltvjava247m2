package vn.t3h.chapter1;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TestClient {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		// Cach 1: tao lop implement (tuong minh)
		Calculator cal = new CalculatorImpl();
		System.out.println(cal.operator(1, 2));
		
		// Cach 2: Dung annonymous inner class
		Calculator cal2 = new Calculator() {
			
			@Override
			public int operator(int a, int b) {
				// TODO Auto-generated method stub
				return a - b;
			}
		};
		System.out.println(cal2.operator(3, 1));
		
		Calculator cal3 = (int a, int b) -> a + b;
		System.out.println(cal3.operator(1, 2));
		

		Calculator cal4 = (a, b) -> a - b;
		System.out.println(cal4.operator(1, 2));
		
		Calculator cal5 = (a, b) -> a * b;
		System.out.println(cal5.operator(1, 2));
		
		// Duyet cac phan tu cua List
		List<Integer> lists = Arrays.asList(1, 5, 10, 20, 3, 4);
		lists.forEach(e -> System.out.println(e));
		
		// Tao danh sach sinh vien
		Student st1 = new Student(1, 5);
		Student st2 = new Student(2, 7);
		Student st3 = new Student(3, 6);
		
		List<Student> students = Arrays.asList(st1, st2, st3);
		students.sort((o1, o2) -> (int)(o1.getScore() - o2.getScore()));
		students.forEach(e -> System.out.println(e));
		
	}

}
