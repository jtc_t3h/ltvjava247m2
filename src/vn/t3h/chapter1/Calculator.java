package vn.t3h.chapter1;

@FunctionalInterface
public interface Calculator {

	public int operator(int a, int b);
}