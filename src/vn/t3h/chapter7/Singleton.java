package vn.t3h.chapter7;

public class Singleton {

	private static Singleton instance;
	private int count = 0;
	
	private Singleton() {
		
	}
	
	public static Singleton getInstance() {
		if (instance == null) {
			instance = new Singleton();
		}
		
		return instance;
	}
	
	public int increment() {
		return ++count;
	}
	
}
