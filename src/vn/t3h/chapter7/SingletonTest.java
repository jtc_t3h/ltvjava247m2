package vn.t3h.chapter7;

public class SingletonTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Singleton singleton1 = Singleton.getInstance();
		System.out.println(singleton1.increment());
		
		Singleton singleton2 = Singleton.getInstance();
		System.out.println(singleton2.increment());
	}

}
