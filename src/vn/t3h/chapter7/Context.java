package vn.t3h.chapter7;

public class Context {

	private static Strategy strategy;
	
	public static Strategy getStrategy(int type) {
		if (type == 1) {
			strategy = new ConcreteStrategyA();
		} else {
			strategy = new ConcreteStrategyB();
		}
		return strategy;
	}
}
