package vn.t3h.chapter7;

public class Proxy implements Service {

	private Service service;
	private int type; // = 1 -> goi A, nguoc lai B

	@Override
	public void defaultMethod() {
		// TODO Auto-generated method stub
		// Tuy thuoc dieu kien -> defaultMethod la cua A hoac B
		if (type == 1) {
			service = new ServiceA();
		} else {
			service = new ServiceB();
		}
		service.defaultMethod();
	}
}
