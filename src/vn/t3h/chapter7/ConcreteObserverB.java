package vn.t3h.chapter7;

public class ConcreteObserverB implements Observer{

private Subject subject;
	
	public ConcreteObserverB() {
		// TODO Auto-generated constructor stub
	}
	
	public ConcreteObserverB(Subject subject) {
		this.subject = subject;
		this.subject.registerObserver(this);
	}
	
	@Override
	public void updateState() {
		// TODO Auto-generated method stub
		System.out.println("Observer pattern: B.");
	}

}
