package vn.t3h.chapter7;

public class TestProxy {

	public static void main(String[] args) {
		Service service1 = new ServiceA();
		service1.defaultMethod();
		
		Service service2 = new ServiceB();
		service2.defaultMethod();
		
		Service service3 = new Proxy();
		service3.defaultMethod(); // 
	}
}
