package vn.t3h.chapter7;

public class ConcreteObserverA implements Observer{

	private Subject subject;
	
	public ConcreteObserverA() {
		// TODO Auto-generated constructor stub
	}
	
	public ConcreteObserverA(Subject subject) {
		this.subject = subject;
		this.subject.registerObserver(this);
	}
	
	
	@Override
	public void updateState() {
		System.out.println("Observer pattern: A.");
	}

}
