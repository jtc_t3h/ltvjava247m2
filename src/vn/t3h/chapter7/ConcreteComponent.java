package vn.t3h.chapter7;

public class ConcreteComponent implements Component {

	@Override
	public void operation() {

		System.out.println("Decorator pattern.");
	}

}
