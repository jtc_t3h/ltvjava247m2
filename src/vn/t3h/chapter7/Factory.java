package vn.t3h.chapter7;

public class Factory {

	public Product getProduct(String type) {
		
		if ("A".equalsIgnoreCase(type)) {
//			return new ProductA();
			
			ProductA product = new ProductA();
			product.setP1("a");
			product.setP2("b");
			product.setPn("n");
		}
		if ("B".equalsIgnoreCase(type)) {
			return new ProductB("a","b","n");
		}
		
		return null;
	}
}
