package vn.t3h.chapter7;

import java.util.ArrayList;
import java.util.List;

public class Subject {
	
	private int state;

	private List<Observer> observerCollection = new ArrayList<Observer>();
	
	public void registerObserver(Observer observer) {
		observerCollection.add(observer);
	}
	
	public void unRegisterObserver(Observer observer) {
		observerCollection.remove(observer);
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
		notifyObserver();
	}
	
	
	public void notifyObserver() {
		for (Observer observer: observerCollection) {
			// thuc hien hanh dong
			observer.updateState();
		}
	}
	
}
