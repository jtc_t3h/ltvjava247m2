package vn.t3h.chapter7;

public interface Strategy {

	public void execute();
}
