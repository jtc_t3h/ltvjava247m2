package vn.t3h.chapter7;

public class TestDecorator {

	public static void main(String[] args) {
//		Component component1 = new ConcreteComponent();
//		component1.operation();
//		
//		Component component2 = new ConcreteDecorator(component1);
//		component2.operation();
		
		LibraryItem item = new Book();
		item.display();
		
		LibraryItem item2 = new BookDecorator("Nguyen Van A", item);
		item2.display();
	}
}
