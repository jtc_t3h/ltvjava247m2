package vn.t3h.chapter7;

public class ConcreteDecorator extends Decorator{


	public ConcreteDecorator(Component component) {
		super(component);
	}

	public void addBehavior() {
		System.out.println("add behavior");
	}

	@Override
	public void operation() {
		component.operation();
		addBehavior();
	}
}
