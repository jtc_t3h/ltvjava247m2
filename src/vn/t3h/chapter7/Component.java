package vn.t3h.chapter7;

public interface Component {

	public void operation();
}
