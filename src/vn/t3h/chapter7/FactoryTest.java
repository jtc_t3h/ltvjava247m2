package vn.t3h.chapter7;

public class FactoryTest {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Factory factory = new Factory();
		Product product = factory.getProduct("A");
		product.create();
		
		product = factory.getProduct("B");
		product.create();
	}

}
