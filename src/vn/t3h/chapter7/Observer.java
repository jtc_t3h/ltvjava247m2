package vn.t3h.chapter7;

public interface Observer {
	
	public void updateState();
}
