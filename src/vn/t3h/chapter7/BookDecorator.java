package vn.t3h.chapter7;

public class BookDecorator implements LibraryItem{

	private String borrower;
	private LibraryItem libraryItem;
	
	public BookDecorator(String borrower, LibraryItem libraryItem) {
		this.borrower = borrower;
		this.libraryItem = libraryItem;
	}



	@Override
	public void display() {
		libraryItem.display();
		System.out.println("Borrower : " + borrower);
	}

}
