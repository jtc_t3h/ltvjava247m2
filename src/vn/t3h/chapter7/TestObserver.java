package vn.t3h.chapter7;

public class TestObserver {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
//		Observer observerA = new ConcreteObserverA();
//		Observer observerB = new ConcreteObserverB();
//		
//		Subject subject = new Subject();
//		subject.registerObserver(observerA);
//		subject.registerObserver(observerB);
//		subject.setState(10);
		
		Subject subject = new Subject();
		new ConcreteObserverA(subject);
		new ConcreteObserverB(subject);
		subject.setState(10);
	}

}
