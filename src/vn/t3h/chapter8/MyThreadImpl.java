package vn.t3h.chapter8;

import java.util.Scanner;

public class MyThreadImpl implements Runnable {

	private String name;
	
	public MyThreadImpl(String name) {
		// TODO Auto-generated constructor stub
		this.name = name;
	}
	
	@Override
	public void run() {
		int n = 0;
		Scanner sc = new Scanner(System.in);
		System.out.print("Nhap N: ");
		n = sc.nextInt();
		
		// TODO Auto-generated method stub
		for (int i = 1; i <= n; i++) {
			System.out.println(this.name + ":" + i);
		}
	}

}
