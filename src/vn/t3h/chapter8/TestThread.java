package vn.t3h.chapter8;

public class TestThread {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// B2: Tao thread moi 
		MyThread t1 = new MyThread();
		t1.setName("MyThread");
		
		// B3: Thuc thi luong moi
		t1.start();
		
		MyThreadImpl myThreadImpl = new MyThreadImpl("MyThreadImpl");
		Thread t2 = new Thread(myThreadImpl);
		t2.start();
		try {
			t2.join();
		} catch (InterruptedException e1) {
			e1.printStackTrace();
		}
		
		// Java 8 -> Runable la FunctionInterface -> Annonymous inner class
		Runnable runnable = new Runnable() {
			
			@Override
			public void run() {
				for (int i = 1; i <= 1000; i++) {
					if (i == 500) {
						try {
							Thread.sleep(2000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					
					System.out.println("Annonymous inner thread" + ":" + i);
				}
			}
		};
		Thread t3 = new Thread(runnable);
		t3.start();
		
		
		// Java 8 -> Runable la FunctionInterface -> dung Lambda expression
		Runnable runnable2 = () -> {
			for (int i = 1; i <= 1000; i++) {
				System.out.println("Lambda expression thread" + ":" + i);
			}
		};
		Thread t4 = new Thread(runnable2);
		t4.setPriority(Thread.MAX_PRIORITY);
		t4.start();
		
		System.out.println("priority of t1: " + t1.getPriority()); // cau lenh thuoc luong nao?
		System.out.println("Done.");
	}

}
