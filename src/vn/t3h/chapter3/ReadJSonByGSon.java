package vn.t3h.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;

import com.google.gson.Gson;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;

public class ReadJSonByGSon {

	public static void main(String[] args) throws JsonSyntaxException, JsonIOException, FileNotFoundException {

		Gson gson = new Gson();
//		Person person = gson.fromJson(new FileReader("src/vn/t3h/chapter3/thongtincanhan.json"), Person.class);
//	
//		System.out.println(person.getName());
//		for (String e: person.getCourses()) {
//			System.out.println(e);
//		}
		
		Person[] persons = gson.fromJson(new FileReader("src/vn/t3h/chapter3/thongtincanhan2.json"), Person[].class);
		for (Person person: persons) {
			System.out.println(person.getName());
			System.out.println(gson.toJson(person));
		}
	}

}
