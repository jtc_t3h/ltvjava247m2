package vn.t3h.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadJSonBySimple {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

		
		JSONParser parser = new JSONParser();
		Object root = parser.parse(new FileReader("src/vn/t3h/chapter3/thongtincanhan.json"));
		
		JSONObject person = (JSONObject) root;
		System.out.println("name : " + person.get("name"));
		
		JSONArray courses = (JSONArray) person.get("courses");
		for (int idx = 0; idx < courses.size(); idx++) {
			System.out.println(courses.get(idx));
		}
	}

}
