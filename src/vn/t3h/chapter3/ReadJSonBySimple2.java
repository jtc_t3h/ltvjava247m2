package vn.t3h.chapter3;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class ReadJSonBySimple2 {

	public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {

		
		JSONParser parser = new JSONParser();
		JSONArray persons = (JSONArray) parser.parse(new FileReader("src/vn/t3h/chapter3/thongtincanhan2.json"));

		for (int idx = 0; idx < persons.size(); idx++) {
			JSONObject person = (JSONObject) persons.get(idx);
			
			System.out.println(person.get("name"));
		}
	}

}
