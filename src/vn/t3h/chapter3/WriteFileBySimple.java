package vn.t3h.chapter3;

import java.io.FileWriter;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

public class WriteFileBySimple {

	public static void main(String[] args) {

		JSONArray persons = new JSONArray();
		
		for (int i = 0; i < 3; i++) {
			JSONObject person = new JSONObject();
			person.put("name", "Nguyen Van " + i);
			
			persons.add(person);
		}
		
		try (FileWriter out = new FileWriter("src/vn/t3h/chapter3/abc.json")){
			out.write(persons.toJSONString());
			out.flush();
			
			System.out.println("Done.");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
