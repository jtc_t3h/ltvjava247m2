package vn.t3h.chapter4;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class WriteFileXML {

	public static void main(String[] args) throws ParserConfigurationException, TransformerException {
		// TODO Auto-generated method stub
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document document = builder.newDocument();
		
		// tao root node
		Element sinhviensNode = document.createElement("sinhviens");
		document.appendChild(sinhviensNode);
		
		// Tao sinhvien1 node
		Element sinhvien1Node = document.createElement("sinhvien");
		sinhvien1Node.setAttribute("type", "KSTN");
		Element hoten1Node = document.createElement("hoten");
		hoten1Node.setTextContent("Nguyen Van B");
		sinhvien1Node.appendChild(hoten1Node);
		sinhviensNode.appendChild(sinhvien1Node);
		
		// Tao sinhvien2 node
		Element sinhvien2Node = document.createElement("sinhvien");
		sinhvien2Node.setAttribute("type", "KSVP");
		Element hoten2Node = document.createElement("hoten");
		hoten2Node.setTextContent("Nguyen Van C");
		sinhvien2Node.appendChild(hoten2Node) ;
		sinhviensNode.appendChild(sinhvien2Node);
		
		
		// chuyen DOM -> ghi ra file
		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer();
		
		DOMSource source = new DOMSource(document);
		StreamResult destination = new StreamResult("src/vn/t3h/chapter4/output.xml");
		transformer.transform(source, destination);
		
		System.out.println("Done!");
	}

}
