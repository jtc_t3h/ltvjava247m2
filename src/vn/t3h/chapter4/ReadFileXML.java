package vn.t3h.chapter4;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReadFileXML {

	public static void main(String[] args) throws ParserConfigurationException, SAXException, IOException {
		// TODO Auto-generated method stub
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		
		Document document = builder.parse(new File("src/vn/t3h/chapter4/sinhvien.xml"));
		
		Element sinhviens = document.getDocumentElement();
		System.out.println(sinhviens.getNodeName());
		
		NodeList sinhviensNL = sinhviens.getChildNodes();
		System.out.println(sinhviensNL.getLength()); // so luong cac node con
		
		// duyet cac node con
		for (int index = 0; index < sinhviensNL.getLength(); index++) {
			Node child = sinhviensNL.item(index);
			if (child.getNodeName().equals("sinhvien")) {
				
				// duyet cac thuoc tinh cua mot node
				NamedNodeMap attributes =  child.getAttributes();
				for(int idx2 = 0; idx2 < attributes.getLength(); idx2++) {
					Node attribute = attributes.item(idx2);
					System.out.println(attribute.getNodeName() + "=" + attribute.getNodeValue());
				}
				
				NodeList sinhvienNL = child.getChildNodes();
				for (int idx = 0; idx < sinhvienNL.getLength(); idx++) {
					Node child2 = sinhvienNL.item(idx);
					
					if (child2.getNodeName().equals("hoten")) {
						System.out.println(child2.getTextContent());
					}
				}
			}
		}
		
		// Hien thi danh sach ten sinh vien
		NodeList hotenNL = document.getElementsByTagName("hoten");
		for (int idx = 0; idx < hotenNL.getLength(); idx++) {
			System.out.println(hotenNL.item(idx).getTextContent());
		}
		
	}

}
