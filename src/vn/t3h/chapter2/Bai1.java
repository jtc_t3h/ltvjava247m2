package vn.t3h.chapter2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Bai1 {

	public static void main(String[] args) {

		// Tao ra danh sach co 15 phan tu (mang tinh hoac Java List)
		String[] list1 = {"hello", "world", "java", "programming", "language", "", "t3h", "python", "lambda", "stream", "ajava"};
//		List<String> list1 = new ArrayList<String>();
		
		for (String e: list1) {
			System.out.println(e);
		}
		System.out.println("==================");
		List<String> list1_ext = Arrays.asList(list1);
		list1_ext.forEach(e -> System.out.println(e));
		
		Stream<String> s1 = Stream.of(list1);
		System.out.println("SL phan tu rong: " + s1.filter(e -> e.isEmpty()).count());
		
		Stream<String> s2 = list1_ext.stream();
		System.out.println("SL phan tu co chieu dai >= 5: " + s2.filter(e -> e.length() >= 5).count());
		
		System.out.println("SL phan tu bat dau ki tu a: " + Stream.of(list1).filter(e -> e.startsWith("a")).count());
		
		
		List<String> list2 = list1_ext.stream().filter(e -> !e.isEmpty()).collect(Collectors.toList());
		list2.forEach(e -> System.out.println(e));
		list2.stream().forEach(e -> System.out.println(e));
		
		String list3 = list1_ext.stream().filter(e -> e.length() >= 3 && e.length() <= 6).collect(Collectors.joining(","));
		System.out.println(list3);
		
		List<String> list4 = list1_ext.stream().map(e -> e + "happy").collect(Collectors.toList());
		list4.forEach(e -> System.out.println(e));
	}

}
