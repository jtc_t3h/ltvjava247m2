package vn.t3h.chapter6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class UserNameValidate {

	public static void main(String[] args) {

		String sPattern = "^[a-z0-9_-]{6,20}$";
		Pattern p = Pattern.compile(sPattern);
		
		String input = "de_pham";
		Matcher m = p.matcher(input);
		
		System.out.println(m.find());
		System.out.println(m.matches());
		
	}

}
