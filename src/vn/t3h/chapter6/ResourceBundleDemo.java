package vn.t3h.chapter6;

import java.util.Locale;
import java.util.ResourceBundle;

public class ResourceBundleDemo {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Locale locale = new Locale("en", "US");
		
//		ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.chapter6.ApplicationResource", locale);
		ResourceBundle bundle = ResourceBundle.getBundle("vn.t3h.chapter6.ResourceBundle", locale);
		
		// lay noi dung trong basename
		System.out.println(bundle.getString("username"));
		System.out.println(bundle.getString("password"));
	}

}
