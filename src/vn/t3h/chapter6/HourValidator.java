package vn.t3h.chapter6;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HourValidator {

	public static void main(String[] args) {

		String sPattern = "([01]?[0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9])";
		Pattern p = Pattern.compile(sPattern);
		
		String input = "29:30:59";
		Matcher m = p.matcher(input);
		
		System.out.println(m.find());
		System.out.println(m.matches());
		
	}

}
