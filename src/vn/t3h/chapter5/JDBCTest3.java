package vn.t3h.chapter5;

import java.sql.*;

public class JDBCTest3 {

	public static void main(String[] args) {
		
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam"; // URL tuong ung voi DBMS khac nhau
		String username = "root";
		String password = "";
		String sql = "select * from cong_ty";
		
		// Java -> try with resources
		try (Connection conn = DriverManager.getConnection(url, username, password);
			 Statement st = conn.createStatement();
			ResultSet rs = st.executeQuery(sql)){
			
			// B6: Xu ly ket qua -> xu ly doi tuong ResultSet
			while (rs.next()) {
				System.out.println("ID = " + rs.getInt(1));
				System.out.println("TEN = " + rs.getString("Ten"));
			}
			
			System.out.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}

}
