package vn.t3h.chapter5;

import java.sql.*;

public class JDBCTest2 {

	public static void main(String[] args) {
		
		// B1: Import thu vien va kiem tra import hay chua? (optional)
		try {
			Class.forName("com.mysql.cj.jdbc.Driver"); // lop Driver tuong ung voi DBMS khac nhau
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// B2: dinh nghia thong so ket noi (url, username va password)
		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam"; // URL tuong ung voi DBMS khac nhau
		String username = "root";
		String password = "";
		
		Connection conn = null;
		Statement st = null;
		ResultSet rs = null;
		try {
			// B3: Tao ket noi = open DB
			conn = DriverManager.getConnection(url, username, password);
			if (conn != null) {
				System.out.println("connect successful.");
			} else {
				System.out.println("connect failed.");
			}
			
			// B4: Tao doi tuong Statement -> quan ly viec thuc thi truy van (executeQuery | executeUpdate)
			st = conn.createStatement();
			
			// B5: Thuc thi truy van
			String sql = "select * from cong_tyaa";
			rs = st.executeQuery(sql);
			
			// B6: Xu ly ket qua -> xu ly doi tuong ResultSet
			while (rs.next()) {
				System.out.println("ID = " + rs.getInt(1));
				System.out.println("TEN = " + rs.getString("Ten"));
			}
			
			
			
			System.out.println("Done!");
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// B7: dong ket noi = close DB
			try {
				rs.close();
				st.close(); 
				conn.close();
			} catch (Exception e1) {
				e1.printStackTrace();
			}
						
		}

	}

}
