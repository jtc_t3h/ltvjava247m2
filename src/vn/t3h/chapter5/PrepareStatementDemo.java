package vn.t3h.chapter5;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

public class PrepareStatementDemo {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub

		String url = "jdbc:mysql://localhost:3306/phan_cong_nhan_vien_1_1_nam"; // URL tuong ung voi DBMS khac nhau
		String username = "root";
		String password = "";
		
		Connection conn = DriverManager.getConnection(url, username, password); 
		conn.setAutoCommit(false);
		
//		Statement st = conn.createStatement();
		String sql = "insert into don_vi (ten) values (?)";
		PreparedStatement ps = conn.prepareStatement(sql);
		
//		String sql1 = "insert into don_vi (ten) values ('Don vi G')";
//		int rowCount = st.executeUpdate(sql1);
		ps.setString(1, "Don vi G");
		int rowCount = ps.executeUpdate();
		
//		String sql2 = "insert into don_vi (ten) values ('Don vi H')";
//		rowCount = st.executeUpdate(sql2);
		ps.setString(1, "Don vi H");
		rowCount += ps.executeUpdate();
		
		if (rowCount == 2) {
			System.out.println("Insert successful.");
		} else {
			System.out.println("Insert failed.");
		}
		
		conn.commit();
		
//		st.close();
		ps.close();
		conn.close();
	}

}
